export const GUILD_NAME: string = process.env.GUILD_NAME || ""
export const PREFIX: string = process.env.PREFIX || "!"
export const TOKEN: string = process.env.TOKEN || ""
export const DBLTOKEN: string = process.env.DBLTOKEN || ""
export const BOT_OWNER: string = process.env.BOT_OWNER || "125492204234997761"
export const BETA: string = process.env.BETA || '0'